#!/bin/bash

stdbuf -o0 ./constant_read | awk -v seedVal=`date +%s` -W interactive '
  function startFile(prefix, cur, max) {
    print prefix cur " ";
    file=prefix cur ".mp3"
    cur = cur + 1;
    if (cur > max) { cur=0; }

    return cur;
  }
BEGIN {
  #srand(srand()+seedVal); 
  #r=rand()
  #minion=int(r * 2);
  minion=0;
  #print "minionVal: " minion "|r: " r
  play=0;

}
/856a482a/ { print "frozen "; play=1; file="--skip 100 frozen.mp3" ; }
/a5ce2b2a/ { print "moana "; play=1; file="moana.mp3" ; }
/05d0452a/ { print "elena "; play=1; file="elena.mp3" ; }
/257e382a/ { minion=startFile("minions", minion, 1); play=1; }
#               print "minion" minion " "; 
#               play=1; file="minions" minion ".mp3" ;
#               minion=minion + 1 ; 
#               if (minion > 1) { minion=0; }
#}
/.*/ { print "stop "; stop=1; }
{
  if (play) {
    system("killall -q mpg321");
    system("mpg321 -q -g 100 " file " & ");
  } else if (stop) {
    system("killall -q mpg321"); 
  }
  stop=0 ;
  play=0 ;
}

'
