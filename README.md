## Introduction
This is a little song player I built after seeing a similar version in action (https://anisse.astier.eu/awk-driven-iot.html). It uses the PN532 RFID reader from AdaFruit and multiple RFID tags to play different songs when a specific tag is read. I have this running on a Raspberry Pi, but there is no reason this couldn't run on another computer. However, you would have to edit the rfid_play.service file to change the user and building libnfc for your distro would be different.

## Pre-Reqs

Install libnfc and if using the Raspberry PI enable UART: https://learn.adafruit.com/adafruit-nfc-rfid-on-raspberry-pi?view=all#freeing-uart-on-the-pi
That site says to download a tar of the libnfc project, but I just cloned the git repo from here: https://github.com/nfc-tools/libnfc


## Compiling

    gcc -o constant_read constant_read.c -lnfc

## Installing Service

    sudo systemctl enable /full/path/rfid_play/rfid_play.service
    sudo systemctl daemon-reload
    sudo systemctl start rfid_play

## Check on Service Status

    sudo systemctl status rfid_play


## Required files

This relies on two audio files so far: frozen.mp3 and moana.mp3. The RFID IDs of their 
respective tags would also need to be obtained. You can get these by running the 
`./constant_read` and scanning the tags. Their IDs will be output into the command line;
take them and edit the rfid_play.sh script and replace the existing with your own and add
more lines to recognize more tags and play different songs. Be sure to restart the service
after changing the shell script though.

## Issues

I had a problem with the audio volume so changed it to 100% with this command:

    sudo amixer set PCM -- 100%

## Images

[![Top](doc_images/top_view_th.jpg)](doc_images/top_view_s.jpg)
[![Left](doc_images/left_side_th.jpg)](doc_images/left_side_s.jpg)
[![Right](doc_images/right_side_th.jpg)](doc_images/right_side_s.jpg)
[![Opened](doc_images/box_opened_th.jpg)](doc_images/box_opened_s.jpg)
[![Closed](doc_images/box_closed_th.jpg)](doc_images/box_closed_s.jpg)

Enjoy.
